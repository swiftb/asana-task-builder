#API documentation: https://developers.asana.com/docs

#TODO
    # bulkify
    # Set Order (try https://developers.asana.com/docs/create-a-subtask) or just reverse the array
    # make opt_fields a param?
    # Err handle each callout
    # handle no matching task
    # handle missing params
    # nested subtasks?
    # can we get a task by name? 
        # looks like we can do it by tag and then just keep the 1st returned 
            # https://developers.asana.com/docs/get-multiple-tasks 
            # https://developers.asana.com/docs/get-tasks-from-a-tag 
        # premium filters: https://developers.asana.com/docs/search-tasks-in-a-workspace
    # can we post a list of tasks to save callouts?
        # premium?
    # due dates?
    # live tests
        # custom fields
        # other?
    # unit tests
    # dependencies
    # req heroku auth
    # set up asana webhook to do away with zapier entirely
        # https://developers.asana.com/docs/webhooks
    # return a better response for endpoint

from flask import Flask, request, jsonify
import requests
import json

set_tasks = Flask(__name__)

@set_tasks.route('/settasks/', methods=['GET'])
def respond():

    # retrieve the projId param
    projId = request.args.get("projid", None)

    # retrieve the code param
    code = request.args.get("code", None)

    # Retrieve the tag parameter
    tag = request.args.get("tag", None)

    # Retrieve the target task
    taskId = request.args.get("taskid", None)

    # Callout to get tasks in project
    projTasksEnd = "https://app.asana.com/api/1.0/tasks"
    authHeader = {"authorization" : "Bearer {}".format(code)}
    projTasksParams = {"project" : projId}
        
    projTasks = requests.get(projTasksEnd,headers=authHeader,params=projTasksParams)
    
    tempTaskId = ""

    # loop tasks to find one with name matching tag
    for theTask in projTasks.json()["data"]:
        if theTask["name"] == tag:
            tempTaskId = theTask["gid"]
            break            
    
    # callout to get sub tasks
    subTasksEnd = "https://app.asana.com/api/1.0/tasks/{}/subtasks".format(tempTaskId)
    subTasksParams = {"opt_fields" : "name,notes"}
    subTasks = requests.get(subTasksEnd,headers=authHeader,params = subTasksParams)
    
    #post the new sub tasks
    finishMsg = "<p>Successfully created the following: </p><ul>"
    jsonString = "<p>Full JSON for each new Task: </p><ul>"
    for newTask in subTasks.json()["data"]:
        newTask.pop("gid",None)
        newTask["parent"] = taskId
        options = {}
        options["pretty"] = "true"
        #options["fields"] = ["name"]
        theBody = {}
        theBody["data"] = newTask
        #theBody["options"] = options        
        postTaskEnd = "https://app.asana.com/api/1.0/tasks"
        postTask = requests.post(postTaskEnd,headers=authHeader,json=theBody)
        finishMsg += "<li>"+newTask["name"]+"</li>"
        jsonString += "<li>"+json.dumps(postTask.json(), indent=4)+"</li>"

    finishMsg += "</ul>"
    jsonString += "</ul>"
    finishMsg += jsonString
    return finishMsg
    
# A welcome message to test our server
@set_tasks.route('/')
def index():
    msg = "<h1>Welcome to the Asana Subtask Copier</h1>"
    msg +="<p>This app will get Subtasks from a Task matched on Name and ProjectId and then copy them to a new Task by provided TaskId</p>"
    msg +="<p>Use this path: /settasks/?tag={TAG}&taskid={TASKID}&projid={PROJECTID}&code={CODE}</p>"
    msg +="<p>Set the following params in the url</p><ul>"
    msg +="<li>tag: the Name of the Task to copy Subtasks from</li>"
    msg +="<li>taskid: the Id of the Task to copy Subtasks to</li>"
    msg +="<li>projid: the Id of the Project to search for the Task to copy from</li>"
    msg +="<li>code: your Asana Personal Access Token (<a href=\"https://developers.asana.com/docs/personal-access-token\" target=\"_blank\">instructions here</a>)</li></ul>"
    msg +="<p><a href=\"https://swiftb@bitbucket.org/swiftb/asana-task-builder.git\" target=\"_blank\">Bitbucket repo</a></p>"
    return msg

if __name__ == '__main__':
    # Threaded option to enable multiple instances for multiple user access support
    set_tasks.run(threaded=True, port=5000)